package cn.smegz.clickhouse

import javax.persistence._

import scala.beans.BeanProperty



@Entity
@IdClass(value = classOf[FWithdraw])
@Table(name = "f_withdraw")
class FWithdraw extends Serializable {
  @Id
  @BeanProperty
  var qdw_id : String = _
  @Id
  @BeanProperty
  var age : Int = _
  @Id
  @BeanProperty
  var gender : Int = _
  @Id
  @BeanProperty
  var amount : Double = _
  @Id
  @BeanProperty
  var pass_amount : Double = _
  @Id
  @BeanProperty
  var i_count : Int = _
  @Id
  @BeanProperty
  var pass_count : Int = _
  @Id
  @BeanProperty
  var emp_id : Int = _
  @Id
  @BeanProperty
  var emp_name : String = _
  @Id
  @BeanProperty
  var dep_id : Int = _
  @Id
  @BeanProperty
  var dep_name : String = _
  @Id
  @BeanProperty
  var client : String = _
  @Id
  @BeanProperty
  var d_date : String = _
  @Id
  @BeanProperty
  var day_of_month : Int = _
  @Id
  @BeanProperty
  var week_of_month : Int = _
  @Id
  @BeanProperty
  var month_of_year : Int = _
  @Id
  @BeanProperty
  var quarter : Int = _
  @Id
  @BeanProperty
  var batch : String = _

  override def toString = s"FWithdraw($qdw_id, $age, $gender, $amount, $pass_amount, $i_count, $pass_count, $emp_id, $emp_name, $dep_id, $dep_name, $client, $d_date)"
}
