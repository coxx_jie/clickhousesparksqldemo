package cn.smegz.spark

import java.util

import org.apache.spark.SparkConf
import org.springframework.boot.context.properties.ConfigurationProperties

import scala.beans.BeanProperty
import scala.collection.JavaConverters._

@ConfigurationProperties(prefix = "spark.config")
class SpringSparkConfig {

  @BeanProperty
  var master : String = "local[*]"
  @BeanProperty
  var appName : String = "test"
  @BeanProperty
  var jarPath : util.List[String] = new util.ArrayList[String]()
  @BeanProperty
  var enableHiveSupport : Boolean = false
  @BeanProperty
  var properties : util.Map[String, String] = new util.HashMap[String, String]()

  def getSparkConf : SparkConf =
      new SparkConf().setAppName(appName).setMaster(master)
      .setJars(jarPath.asScala).setAll(properties.asScala)
}
