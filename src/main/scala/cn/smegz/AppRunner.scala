package cn.smegz

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.transaction.annotation.EnableTransactionManagement

object AppRunner extends App {
  SpringApplication.run(classOf[AppRunner])
}

@EnableTransactionManagement
@SpringBootApplication
class AppRunner
