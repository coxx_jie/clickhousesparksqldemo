package cn.smegz.clickhouse

import org.springframework.data.jpa.repository.JpaRepository

trait FWithdrawRepo extends JpaRepository[FWithdraw, String]
