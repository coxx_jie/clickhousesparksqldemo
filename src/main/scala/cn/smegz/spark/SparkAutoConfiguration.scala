package cn.smegz.spark

import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.{Bean, Configuration}

@EnableConfigurationProperties(value = Array(classOf[SpringSparkConfig]))
@Configuration
class SparkAutoConfiguration {

    @Autowired
    var springSparkConfig : SpringSparkConfig = _
    @Bean
    def createSparkContext() : SparkContext =
        SparkContext.getOrCreate(springSparkConfig.getSparkConf)
    @Bean
    def createSparkSession() : SparkSession = {
        val sparkSessionBuilder = SparkSession.builder().config(springSparkConfig.getSparkConf)
        if(springSparkConfig.enableHiveSupport) sparkSessionBuilder.enableHiveSupport().getOrCreate()
        else sparkSessionBuilder.getOrCreate()
    }

}
