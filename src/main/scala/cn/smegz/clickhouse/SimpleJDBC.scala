package cn.smegz.clickhouse

import java.sql.DriverManager

import org.springframework.util.ClassUtils

import scala.beans.BeanProperty
import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

class SimpleJDBC {
  @BeanProperty
  var address : String = "jdbc:clickhouse://172.16.1.23:8123/default"
  @BeanProperty
  var username : String = "root"
  @BeanProperty
  var password : String = "123"
  @BeanProperty
  var driverClass : String = "ru.yandex.clickhouse.ClickHouseDriver"
  @BeanProperty
  var sql : String = _

  //简单查询
  def simpleQuerySQLDemo(): List[mutable.Map[String, Any]] = {
    ClassUtils.forName(driverClass, null)
    val connection = DriverManager.getConnection(address, username, password)
    val preStatement = connection.prepareStatement(sql)
    val resultSet = preStatement.executeQuery()
    val resultSetMetaData = preStatement.getParameterMetaData
    val resultList = new ArrayBuffer[mutable.Map[String, Any]]()
    while(resultSet.next()){
      val map = new mutable.HashMap[String, Any]()
      for(i <- 0 until resultSetMetaData.getParameterCount){
        map += resultSetMetaData.getParameterClassName(i) -> resultSet.getObject(i)
      }
      resultList += map
    }
    resultList.toList
  }
}
